"""
    Creator: Matteo Ceradini
"""

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import os.path
import numpy as np
np.random.seed(1234)

from utils.videofile_reader import *

def validate_model(validation_video, dlc_model, student_model, batch_size):
    total_loss = 0
    val_batch_ind = 0

    validation_cap, validation_nframes, validation_frame_width, validation_frame_height = open_video(validation_video, False)
    val_frame = np.empty((batch_size, validation_frame_height, validation_frame_width, 3), dtype="ubyte") 

    while validation_cap.isOpened():
        temp_ret, temp_frame = validation_cap.read()
        
        if temp_ret:
            val_frame[val_batch_ind] = img_as_ubyte(cv2.cvtColor(temp_frame, cv2.COLOR_BGR2RGB))
            
            if val_batch_ind == batch_size - 1:
                val_batch_ind = 0
                dlc_inference, dlc_pose = dlc_model.inference(val_frame)
                student_pose = student_model.inference(val_frame)
                
                difference = np.abs(np.subtract(dlc_pose[:, :2], student_pose[:, :2])).sum(axis=0).sum()       
                                                                
                total_loss += difference     
            else:
                val_batch_ind += 1 
        else:   
            break             

    print("Video validation loss: {}".format(total_loss))
    
    return total_loss

def validate_model_static(validation_video, dlc_results, student_model, batch_size):
    total_loss = 0
    batch_ind = 0
    batch_num = 0

    validation_cap, validation_nframes, validation_frame_width, validation_frame_height = open_video(validation_video, False)
    val_frame = np.empty((batch_size, validation_frame_height, validation_frame_width, 3), dtype="ubyte") 

    while validation_cap.isOpened():
        temp_ret, temp_frame = validation_cap.read()
        
        if temp_ret:
            val_frame[batch_ind] = img_as_ubyte(cv2.cvtColor(temp_frame, cv2.COLOR_BGR2RGB))
            
            if batch_ind == batch_size - 1:
                dlc_inference = dlc_results['part_pred'][batch_ind][batch_num * batch_size : batch_num * batch_size + batch_ind + 1]
                dlc_pose = dlc_results['pose'][batch_num * batch_size : batch_num * batch_size + batch_ind + 1]

                student_pose = student_model.inference(val_frame)
                
                difference = np.abs(np.subtract(dlc_pose[:, :2], student_pose[:, :2])).sum(axis=0).sum()      
                                                                
                total_loss += difference 

                batch_ind = 0 
                batch_num += 1  
            else:
                batch_ind += 1 
        else:   
            break             

    print("Video validation loss: {}".format(total_loss))
    
    return total_loss