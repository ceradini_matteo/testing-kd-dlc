"""
    Creator: Matteo Ceradini
"""

import cv2
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import numpy as np
np.random.seed(1234)
from tqdm import tqdm
from skimage.util import img_as_ubyte

def open_video(file_name, debug=True):
    cap = cv2.VideoCapture(file_name)

    if not cap.isOpened():
        raise IOError("Video ({}) could not be opened. Please check the file integrity.".format(file_name))

    fps = cap.get(5)
    nframes = int(cap.get(7))
    duration = nframes * 1.0 / fps
    height = int(cap.get(4))
    width = int(cap.get(3))

    if debug:
        # debug info about the video on the console
        print("Duration of video [s]: {} - recorded with {} fps".format(round(duration, 2), round(fps, 2)))
        print("Overall # of frames: {} - found with (before cropping) frame dimensions: {}x{}".format(nframes, width, height))

    return cap, nframes, width, height