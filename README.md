Il problema da affrontare è quello della pose prediction del naso del topo in un video: ho un modello ResNet-50 in cui al posto dei due layer fully-connected finali per la classificazione c'è un una layer deconvoluzionale (conv-transpose) che da in output una matrice sostanzialmente, da questa matrice con delle operazioni Numpy praticamente si va ad estrarre le coordinate e la probabilità di accuratezza di questa posizione.

Per cercare di ridurre le dimensioni del modello una delle tecniche che ho applicato si chiama knowledge-distillation, dove vado ad allenare un modello di rete più piccolo (lo student) sui risultati del modello più grande (il teacher). Come modello più piccolo ho utilizzato sempre una ResNet, ma a cui ho troncato i due blocchi di layer conv finali. Il training viene fatto trasferendo tutti i pesi dei primi due blocchi dal modello teacher a quello student e mettendoli freeze durante l'allenamento, poi aggiungo solamente un layer convoluzionale ed il solito layer deconvoluzionale. In pratica solo questi ultimi due vengono allenati durante il training.
La funzione di loss viene calcolata sulla differenza tra i tensori risultati dalla deconvoluzione del teacher e dello student.

Applicando questa tecnica con Tensorflow riesco ad avere dei risultati di approssimazione accettabili pur avendo un modello grande la metà praticamente.

Ora vorrei portare questo modello su Pytorch, perché si è dimostrato andare molto più veloce (sopratutto sulla schedina embedded). Le strade che posso applicare sono 2:

1. trasferire i pesi;
2. creare un modello student con Pytorch ed allenarlo sui risultati del teacher.

La 1. si è rivelata non funzionare: portando tutti i pesi da Tensorflow a Pytorch sul medesimo modello resta una piccolissima differenza (che credo sia dovuta a piccoli dettagli di implementazione delle convoluzioni) nel risultato del tensore dalla deconvoluzione (dell'ordine di tipo 0.1 su un tensore grande 8x64x50), questa piccola differenza poi quando si va ad estrarre la posizione target diventa piuttosto significativa al punto da rendere praticamente inutilizzabili, o quasi, i risultati.

Anche nella 2. come ti dicevo mi sono ritrovato bloccato. Ho creato lo stesso modello ResNet troncato a due soli blocchi convoluzionali, aggiunto il layer convoluzionale e quello deconv e fatto il transfer learning dal modello teacher. Impostato stessi parametri di learning e medesima loss function però niente: il modello student Pytorch non riesce ad approssimare quello teacher in Tensorflow.

Ancora grazie per l'aiuto.

Matteo.