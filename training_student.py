"""
    Creator: Matteo Ceradini
"""

seed_value = 1234

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['PYTHONHASHSEED'] = str(seed_value)

import os.path
import argparse
import time
import pandas as pd
import random
random.seed(seed_value)
import numpy as np
np.random.seed(seed_value)
from tqdm import tqdm
from pathlib import Path

import tensorflow as tf
tf.get_logger().setLevel('ERROR')
tf.compat.v1.set_random_seed(seed_value)
tf.set_random_seed(seed_value)

import torch
torch.manual_seed(seed_value)

# from dlc_model import *
from student_model_pytorch import *
from utils.videofile_reader import *
from utils.training import *

print("+-----------------------------------------------+","\n|\tTensorflow version:\t", tf.__version__, "\t|")
print("|\tGPU is available:\t", tf.test.is_gpu_available(), "\t\t|\n+-----------------------------------------------+")
print("+-----------------------------------------------+","\n|\tPyTorch version:   \t", torch.__version__, "\t\t|")
print("|\tGPU is available:\t", torch.cuda.is_available(), "\t\t|\n+-----------------------------------------------+")

# initial configurations
config = 'dlc_config/resnet_50_config.yaml'
test_config = "dlc_config/resnet_50_pose_cfg.yaml"
snapshot = "resnet-50"

batch_size = 8
num_outputs = 1
stride = 8

# training_video = 'video/10_seconds_start_05_00_30fps_500w.mp4'
training_video = 'video/10_minutes_start_05_00_60fps_500w.mp4'
# training_video = 'video/20_minutes_start_05_00_60fps_500w.mp4'
# training_video = 'video/video1_60fps_training.mp4'

# training_video_result = 'predictions/10_seconds_start_05'
training_video_result = 'predictions/10_minutes_start_05'
# training_video_result = 'predictions/20_minutes_start_05'

validation_video = 'video/10_seconds_start_05_00_30fps_500w.mp4'

# initializing the DLC-model
#dlc_model = DlcModel(config, test_config, snapshot)

# loading the DLC-model results
dlc_results = {}

with open("{}_part_pred.npy".format(training_video_result), 'rb') as f:
    dlc_results['part_pred'] = np.load(f)

with open("{}_pose.npy".format(training_video_result), 'rb') as f:
    dlc_results['pose'] = np.load(f)

# parsing terminal parameters
parser = argparse.ArgumentParser()
parser.add_argument('--new', action='store_true', help='Create new weights for the model and not load from previous iteration')
parser_args = parser.parse_args()

new_training = True if parser_args.new else False

# initializing the student model
args = {
    'batch_size': batch_size,
    'num_outputs': num_outputs,
    'stride': stride,
    'mean_pixel': [123.68, 116.779, 103.939],
    'locref_stdev': 7.2801,
    'new_training': new_training
}

student_model = StudentModel(args, training=True)
# student_model.load_model()

print("\nStarting training of the student model using the video: {}".format(training_video))

# prepare frames for training video
cap, nframes, frame_width, frame_height = open_video(training_video)
frames = np.empty((batch_size, frame_height, frame_width, 3), dtype="ubyte") 

pbar = tqdm(total=nframes)
counter = 0
step = max(10, int(nframes / 100))

batch_ind = 0  # keeps track of which image within a batch should be written to
batch_num = 0  # keeps track of which batch you are at

validation_step = 250
start = time.time()

print("\nValidation before training:")
# validate_model_static(validation_video, dlc_results, student_model, batch_size)

while cap.isOpened():
    if counter % step == 0:
        pbar.update(step)

    ret, frame = cap.read()

    if ret:
        frames[batch_ind] = img_as_ubyte(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))

        if batch_ind == batch_size - 1:
            val_cond = batch_num % validation_step == 0 
            dlc_inference = dlc_results['part_pred'][batch_num * batch_size : batch_num * batch_size + batch_ind + 1]
            dlc_pose = dlc_results['pose'][batch_num * batch_size : batch_num * batch_size + batch_ind + 1]

            model_loss = student_model.update_weights(frames, dlc_inference, dlc_pose, val_cond)

            batch_ind = 0
            batch_num += 1

            # validation step
            if val_cond:
                print('Model loss: {}'.format(model_loss))
                total_loss = validate_model_static(validation_video, dlc_results, student_model, batch_size)
        else:
            batch_ind += 1
    else:
        nframes = counter

        ''' TODO: fix indexes in the retrieving from dlc_results
        
        if batch_ind > 0:
            dlc_inference = dlc_results['part_pred'][batch_ind][batch_num * batch_size : batch_num * batch_size + batch_ind + 1]
            dlc_pose = dlc_results['pose'][batch_num * batch_size : batch_num * batch_size + batch_ind + 1]

            student_model.update_weights(frames, dlc_inference, dlc_pose)'''

        stop = time.time()
        print("\nLast validation:")
        validate_model_static(validation_video, dlc_results, student_model, batch_size)

        print("Detected frames: ", nframes)
        print("Time for training: {} m".format(round((stop - start)/ 60 , 2)))
        break

    counter += 1

pbar.close()

print("Saving results")
student_model.save_model()