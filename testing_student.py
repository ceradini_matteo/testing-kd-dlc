"""
    Creator: Matteo Ceradini
"""

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import os.path
import argparse
import time
import pandas as pd
import numpy as np
np.random.seed(1234)
from tqdm import tqdm
from pathlib import Path

import tensorflow as tf
tf.get_logger().setLevel('ERROR')
tf.compat.v1.set_random_seed(1234)

from dlc_utils.make_labeled_video import *
#from student_model import *
from student_model_pytorch import *
from utils.videofile_reader import *

print("+-----------------------------------------------+","\n|\tTensorflow version:\t", tf.__version__, "\t|")
print("|\tGPU is available:\t", tf.test.is_gpu_available(), "\t\t|\n+-----------------------------------------------+")

# initial configurations
config = 'dlc_config/resnet_50_config.yaml'
test_config = "dlc_config/resnet_50_pose_cfg.yaml"
snapshot = "resnet-50"

batch_size = 8
num_outputs = 1
stride = 8

# initializing the student model
args = {
    'batch_size': batch_size,
    'num_outputs': num_outputs,
    'stride': stride,
    'mean_pixel': [123.68, 116.779, 103.939],
    'locref_stdev': 7.2801,
    'new_training': False
}

parser = argparse.ArgumentParser()
parser.add_argument('--model', default="mini_resnet_v1", help='Name of the model to validate')
parser_args = parser.parse_args()
model = parser_args.model

student_model = StudentModel(args, model_name=model, pose_estimation=True)
student_model.load_model()

video_to_test = ['video/10_seconds_start_05_00_30fps_500w.mp4','video/1_minute_start_00_00_500w.mp4','video/1_minute_start_00_23_00_60fps_500w.mp4']
result_names = ['student_benchmark_10_seconds','student_benchmark_1_minute','student_benchmark_1_minute_s_23']

# video = ['video/video1_60fps_training_30m_500w.mp4']
# result_names = ['student_benchmark_30_minutes']

for video_index, video in enumerate(video_to_test):
    cap, nframes, frame_width, frame_height = open_video(video)

    frames = np.empty((batch_size, frame_height, frame_width, 3), dtype="ubyte")  
    pbar = tqdm(total=nframes)
    counter = 0
    step = max(10, int(nframes / 100))

    batch_ind = 0  # keeps track of which image within a batch should be written to
    batch_num = 0  # keeps track of which batch you are at

    start = time.time()
    stop = 0

    PredictedData = np.zeros((nframes, 3 * num_outputs))

    while cap.isOpened():
        if counter % step == 0:
            pbar.update(step)

        ret, frame = cap.read()

        if ret:
            frames[batch_ind] = img_as_ubyte(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))

            if batch_ind == batch_size - 1:
                student_pose = student_model.inference(frames)
                PredictedData[batch_num * batch_size : (batch_num + 1) * batch_size, :] = student_pose

                batch_ind = 0
                batch_num += 1

            else:
                batch_ind += 1
        else:
            nframes = counter

            stop = time.time()
            print("Detected frames: ", nframes)
            print("Time for inference: {} s".format(round(stop - start)))
            break

        counter += 1

    dest_folder = 'video/result'
    dest_name = result_names[video_index]

    print("Saving results in {}/{}:".format(dest_folder, dest_name))

    dictionary = {
        "start": start,
        "stop": stop,
        "run_duration": stop - start,
        "DLC-model-config file": test_config,
        "batch_size": args["batch_size"],
        "frame_dimensions": (frame_height, frame_width),
        "nframes": nframes
    }
    metadata = {"data": dictionary}

    data_name = os.path.join(dest_folder, dest_name + ".h5")

    xyz_labs = ["x", "y", "likelihood"]

    pdindex = pd.MultiIndex.from_product(
        [["dlc_resnet"], ["part1"], xyz_labs],
        names=["scorer", "bodyparts", "coords"],
    )

    save_as_csv = True

    auxiliaryfunctions.SaveData(
        PredictedData[:nframes, :],
        metadata,
        data_name,
        pdindex,
        range(nframes),
        save_as_csv,
    )

    # creation of labeled video
    evaluation_file = 'video/result/{}.h5'.format(result_names[video_index])   

    create_labeled_video(config, video, evaluation_file)