"""
    Creator: Matteo Ceradini
"""

seed_value = 1234

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['PYTHONHASHSEED'] = str(seed_value)

import random
random.seed(seed_value)

import numpy as np
np.random.seed(seed_value)

import tf2onnx
import tensorflow as tf
tf.get_logger().setLevel('ERROR')
tf.compat.v1.set_random_seed(seed_value)
tf.set_random_seed(seed_value)

from nets.layers import *
from nets.resnet_block import *
import nets.init.layers_shape_resnet_2_block as layer_shapes

# this uses ResNet-50
class ConvNet:
    def __init__(self, args, model_name):
        tf.reset_default_graph()
        self.batch_size = args['batch_size']
        self.stride = args['stride']
        self.num_outputs = args['num_outputs']
        self.mean_pixel = args['mean_pixel']
        self.locref_stdev = args['locref_stdev']
        self.new_training = args['new_training']
        
        self.initializer = 'resnet-50'

        self.net_weights_initializer = tf.contrib.layers.xavier_initializer()
        self.deconv_weights_initializer = tf.contrib.layers.xavier_initializer()
        
        # define parameters for architecture of the resnet-dlc part
        self.num_blocks = 2
        self.num_block_units = [3,4]
        self.num_block_conv_layers = 3
        self.output_resnet = 512 # number of layers for the output from the resnet-dlc part
        
        self.deconv_weights = self.__create_deconv_weights()
        self.deconv_biases = self.__create_deconv_biases()
        
        self.__create_net_weights()

        self.learning_rate = 0.0001
        self.temp_threshold = 0.8
        self.model_path = "./models/{}".format(model_name)

        self.model = self.__build_model()

        self.prepare_for_pose_estimation()
        self.saver = tf.train.Saver()

    def __build_model(self):
        with tf.variable_scope("student_model") as scope:
            # input
            self.input = tf.placeholder("float", [self.batch_size, None, None, 3], name="input")
            # output
            output = {}

            mean = tf.constant(self.mean_pixel, dtype=tf.float32, shape=[1, 1, 1, 3], name="input_img_mean")
            im_centered = self.input - mean

            # setup new mini ResNet model (from resnet-dlc: 1^ conv freezed + 2 block)
            net = conv2d_batchnorm_relu(im_centered, W=self.net_weights['conv1'], strides=2)
            net = max_pool_2d(net, ksize=3, strides=2)
            net = get_resnet_block(net, reps=3, W=self.net_weights['block1'], strides=1, block_name="block1")
            net = get_resnet_block(net, reps=4, W=self.net_weights['block2'], strides=2, block_name="block2") 
            
            # setup new last layer after resnet part 
            net = conv2d(net, W=self.net_weights['last1']['conv2d'], strides=2, relu=False) 

            output['myconv2d'] = net      

            # deconv layers for output prediction
            self.dlc_part_pred = tf.placeholder("float", [self.batch_size, None, None, self.num_outputs], name="dlc_prediction/part_pred")
            # self.dlc_locref = tf.placeholder("float", [self.batch_size, None, None, self.num_outputs*2], name="dlc_prediction/locref")
            self.dlc_pose = tf.placeholder("float", [self.batch_size, self.num_outputs*3], name="dlc_pose")

            output['part_pred'] = deconv2d(net, self.deconv_weights['deconv/part_pred/weights'], self.deconv_biases['deconv/part_pred/biases'], self.num_outputs, strides=2)
            # output['locref'] = deconv2d(net, self.deconv_weights['deconv/locref/weights'], self.deconv_biases['deconv/locref/biases'], self.num_outputs*2, strides=2)

        return output

    def __create_net_weights(self):
        # define the weight variables
        with tf.variable_scope("student_model") as scope:   
            # setup 1° resnet conv layer         
            net_weights = {
                'conv1':{
                    'conv2d': tf.get_variable('conv1/weights', shape=(7,7,3,64), trainable=False),
                    'batch_norm/moving_mean': tf.get_variable('resnet_v1_50/conv1/batch_norm/moving_mean', shape=(64,), trainable=False),
                    'batch_norm/moving_variance': tf.get_variable('resnet_v1_50/conv1/batch_norm/moving_variance', shape=(64,), trainable=False),
                    'batch_norm/gamma': tf.get_variable('resnet_v1_50/conv1/batch_norm/gamma', shape=(64,), trainable=False),
                    'batch_norm/beta': tf.get_variable('resnet_v1_50/conv1/batch_norm/beta', shape=(64,), trainable=False)
                }
            }
            
            # setup resnet shortcut weights and batchnorm operations
            for b in range(0, self.num_blocks):
                net_weights['block{}'.format(b+1)] = {}

            shortcut_weights_shapes = layer_shapes.get_shortcut_weights_shapes()
            shortcut_batchnorm_shapes = layer_shapes.get_shortcut_batchnorm_shapes()

            for b in range(0, self.num_blocks):
                net_weights['block{}'.format(b+1)]['unit_1/shortcut'] = {}

                net_weights['block{}'.format(b+1)]['unit_1/shortcut']['conv2d'] = tf.get_variable('resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/weights'.format(b+1), shape=shortcut_weights_shapes[b], trainable=False)
                net_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/moving_mean'] = tf.get_variable('resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/batch_norm/moving_mean'.format(b+1), shape=shortcut_batchnorm_shapes[b], trainable=False)
                net_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/moving_variance'] = tf.get_variable('resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/batch_norm/moving_variance'.format(b+1), shape=shortcut_batchnorm_shapes[b], trainable=False)
                net_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/gamma'] = tf.get_variable('resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/batch_norm/gamma'.format(b+1), shape=shortcut_batchnorm_shapes[b], trainable=False)
                net_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/beta'] = tf.get_variable('resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/batch_norm/beta'.format(b+1), shape=shortcut_batchnorm_shapes[b], trainable=False)

            # setup resnet block weights fr convolutions and batchnorm operations
            block_weights_shapes = layer_shapes.get_block_weights_shapes()
            block_batchnorm_shapes = layer_shapes.get_block_batchnorm_shapes()

            for b in range(0, self.num_blocks):
                for u in range(0, self.num_block_units[b]):
                    for c in range(0, self.num_block_conv_layers):
                        net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)] = {}

                        net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['conv2d'] = tf.get_variable('resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/weights'.format(b+1,u+1,c+1), shape=block_weights_shapes[b][u][c], trainable=False)
                        net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/moving_mean'] = tf.get_variable('resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/batch_norm/moving_mean'.format(b+1,u+1,c+1), shape=block_batchnorm_shapes[b][c], trainable=False)
                        net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/moving_variance'] = tf.get_variable('resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/batch_norm/moving_variance'.format(b+1,u+1,c+1), shape=block_batchnorm_shapes[b][c], trainable=False)
                        net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/gamma'] = tf.get_variable('resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/batch_norm/gamma'.format(b+1,u+1,c+1), shape=block_batchnorm_shapes[b][c], trainable=False)
                        net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/beta'] = tf.get_variable('resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/batch_norm/beta'.format(b+1,u+1,c+1), shape=block_batchnorm_shapes[b][c], trainable=False)

            # new last part (after the resnet part) weights
            net_weights['last1'] = {
                'conv2d': tf.get_variable('last_1/conv1/weights', shape=[3,3,self.output_resnet,self.output_resnet*2], initializer=self.net_weights_initializer)
            }
            
            self.net_weights = net_weights

    def __create_deconv_weights(self):
        with tf.variable_scope("student_model") as scope:   
            deconv_weights = {
                'deconv/part_pred/weights': tf.get_variable('deconv/part_pred/weights', shape=(3,3,1,self.output_resnet*2), initializer=self.deconv_weights_initializer),
                # 'deconv/locref/weights': tf.get_variable('deconv/locref/weights', shape=(3,3,2,self.output_resnet*2), initializer=self.deconv_weights_initializer),
            }

        return deconv_weights

    def __create_deconv_biases(self):
        with tf.variable_scope("student_model") as scope:   
            deconv_biases = {
                'deconv/part_pred/biases': tf.get_variable('deconv/part_pred/biases', shape=(1,)),
                # 'deconv/locref/biases': tf.get_variable('deconv/locref/biases', shape=(2,))
            }

        return deconv_biases

    def start_session(self, initialization=True):
        self.sess = tf.Session()

        if initialization:
            self.sess.run(tf.global_variables_initializer())
            self.sess.run(tf.local_variables_initializer())

    def close_session(self):
        self.sess.close()

    def save_model(self):
        self.saver.save(self.sess, self.model_path)

    def load_model(self):
        self.saver.restore(self.sess, self.model_path)
        
    def __load_weights_from_dlc(self):        
        # load weights from the ResNet-DLC pretrained model
        resnet_dlc_model = './dlc_models/{}'.format(self.initializer)
        assign_ops = []
        
        # first layer operations assignments
        conv1_weights = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/conv1/weights')
        assign_ops.append(self.net_weights['conv1']['conv2d'].assign(conv1_weights))
        moving_mean = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/conv1/BatchNorm/moving_mean')
        assign_ops.append(self.net_weights['conv1']['batch_norm/moving_mean'].assign(moving_mean))
        moving_variance = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/conv1/BatchNorm/moving_variance')
        assign_ops.append(self.net_weights['conv1']['batch_norm/moving_variance'].assign(moving_variance))
        batch_norm_gamma = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/conv1/BatchNorm/gamma')
        assign_ops.append(self.net_weights['conv1']['batch_norm/gamma'].assign(batch_norm_gamma))
        batch_norm_beta = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/conv1/BatchNorm/beta')
        assign_ops.append(self.net_weights['conv1']['batch_norm/beta'].assign(batch_norm_beta))
        
        # shortcut assignments
        for b in range(0, self.num_blocks):
            var_weights = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/weights'.format(b+1))
            assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_1/shortcut']['conv2d'].assign(var_weights))
            var_mean = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/BatchNorm/moving_mean'.format(b+1))
            assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/moving_mean'].assign(var_mean))
            var_variance = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/BatchNorm/moving_variance'.format(b+1))
            assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/moving_variance'].assign(var_variance))
            var_gamma = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/BatchNorm/gamma'.format(b+1))
            assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/gamma'].assign(var_gamma))
            var_beta = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/BatchNorm/beta'.format(b+1))
            assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/beta'].assign(var_beta))

        # resnet blocks weights and batchnorm assignments
        for b in range(0, self.num_blocks):
            for u in range(0, self.num_block_units[b]):
                for c in range(0, self.num_block_conv_layers):
                    var_weights = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/weights'.format(b+1,u+1,c+1))
                    assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['conv2d'].assign(var_weights))

                    var_mean = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/BatchNorm/moving_mean'.format(b+1,u+1,c+1))
                    assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/moving_mean'].assign(var_mean))
                    var_variance = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/BatchNorm/moving_variance'.format(b+1,u+1,c+1))
                    assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/moving_variance'].assign(var_variance))
                    var_gamma = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/BatchNorm/gamma'.format(b+1,u+1,c+1))
                    assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/gamma'].assign(var_gamma))
                    var_beta = tf.train.load_variable(resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/BatchNorm/beta'.format(b+1,u+1,c+1))
                    assign_ops.append(self.net_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/beta'].assign(var_beta))
        
        # deconvolution layers assignments
        '''var_weights = tf.train.load_variable(resnet_dlc_model, 'pose/part_pred/block4/weights')
        assign_ops.append(self.deconv_weights['deconv/part_pred/weights'].assign(var_weights))
        var_biases = tf.train.load_variable(resnet_dlc_model, 'pose/part_pred/block4/biases')
        assign_ops.append(self.deconv_biases['deconv/part_pred/biases'].assign(var_biases))

        var_weights = tf.train.load_variable(resnet_dlc_model, 'pose/locref_pred/block4/weights')
        assign_ops.append(self.deconv_weights['deconv/locref/weights'].assign(var_weights))
        var_biases = tf.train.load_variable(resnet_dlc_model, 'pose/locref_pred/block4/biases')
        assign_ops.append(self.deconv_biases['deconv/locref/biases'].assign(var_biases))'''
        
        self.sess.run(assign_ops)

    # define the cost function and the optimizer for the training phase
    def prepare_for_training(self):
        cost_part_pred = tf.reduce_mean(tf.squared_difference(self.model['part_pred'], self.dlc_part_pred))
        # cost_locref = tf.reduce_mean(tf.squared_difference(self.model['locref'], self.dlc_locref))
        
        # definin smooth-l1 loss
        # diff = tf.abs(self.dlc_part_pred-self.model['part_pred'])	    
        # less = tf.less(diff, 1.0)
        # cost_part_pred = (less_than_one*0.5*diff**2)+(1.0-less_than_one)*(diff-0.5)
        
        # self.cost = tf.reduce_mean(0.7*cost_part_pred + 0.3*cost_locref) 
        
        # define the temperature a threshold detector on the average pose probability of DLC
        avg = tf.reduce_mean(self.dlc_pose[:,2]) # average probability of the poses from DLC
        cond = tf.less(avg, self.temp_threshold)
        temperature = tf.where(cond, 0.0, 1.0)
        
        self.cost = temperature*cost_part_pred
	    
        # self.cost = 0.5*cost_part_pred + 0.5*cost_locref
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.cost)
        
        # after the definition of all the graph and of the optimizer we can start the session and load the resnet weights from the dlc-model 
        pre_train_model_path = "{}_pre_train".format(self.model_path) 

        # start session and load all weights
        self.start_session()

        if self.new_training:
            print("Loading weights from ResNet-DLC model")
            self.__load_weights_from_dlc()
            self.saver.save(self.sess, pre_train_model_path)
        else:
            print("Loading weights from previous training ({})".format(pre_train_model_path))
            self.saver.restore(self.sess, pre_train_model_path)            

    def prepare_for_pose_estimation(self):
        # locref = self.model["locref"]
        probs = tf.sigmoid(self.model["part_pred"])

        if self.batch_size == 1:
            # assuming batchsize 1 here!
            probs = tf.squeeze(probs, axis=0)
            # locref = tf.squeeze(locref, axis=0)
            l_shape = tf.shape(probs)

            # locref = tf.reshape(locref, (l_shape[0] * l_shape[1], -1, 2))
            probs = tf.reshape(probs, (l_shape[0] * l_shape[1], -1))
            maxloc = tf.argmax(probs, axis=0)

            loc = tf.unravel_index(maxloc, (tf.cast(l_shape[0], tf.int64), tf.cast(l_shape[1], tf.int64)))
            maxloc = tf.reshape(maxloc, (1, -1))
            joints = tf.reshape(tf.range(0, tf.cast(l_shape[2], dtype=tf.int64)), (1, -1))
        else:
            # probs = tf.squeeze(probs, axis=0)
            #l_shape = tf.shape(probs)  # batchsize times x times y times body parts
            
            # locref = tf.reshape(locref, (l_shape[0], l_shape[1], l_shape[2], l_shape[3], 2))
            # turn into x times y time bs * bpts
            # locref = tf.transpose(locref, [1, 2, 0, 3, 4])
            probs = tf.transpose(probs, [1, 2, 0, 3])

            l_shape = tf.shape(probs)  # x times y times batch times body parts

            # locref = tf.reshape(locref, (l_shape[0] * l_shape[1], -1, 2))
            probs = tf.reshape(probs, (l_shape[0] * l_shape[1], -1))
            maxloc = tf.argmax(probs, axis=0)

            loc = tf.unravel_index(maxloc, (tf.cast(l_shape[0], tf.int64), tf.cast(l_shape[1], tf.int64)))
            maxloc = tf.reshape(maxloc, (1, -1))
            joints = tf.reshape(tf.range(0, tf.cast(l_shape[2] * l_shape[3], dtype=tf.int64)), (1, -1))

        indices = tf.transpose(tf.concat([maxloc, joints], axis=0))

        # offset = tf.gather_nd(locref, indices)
        # offset = tf.gather(offset, [1, 0], axis=1)

        likelihood = tf.reshape(tf.gather_nd(probs, indices), (-1, 1))

        pose = (
            self.stride * tf.cast(tf.transpose(loc), dtype=tf.float32)
            + self.stride * 0.5
            # + offset * self.locref_stdev
        )

        pose = tf.concat([pose, likelihood], axis=1)

        self.pose = pose

    def train(self, x, dlc_prediction, dlc_pose, validation):
        self.sess.run(self.optimizer, feed_dict={
            self.input: x,
            self.dlc_part_pred: dlc_prediction['part_pred'],
            # self.dlc_locref: dlc_prediction['locref']
            self.dlc_pose: dlc_pose
        })

    def inference(self, x):
        pose = self.sess.run(self.pose, feed_dict={self.input: x})

        pose[:, [0, 1, 2]] = pose[:, [1, 0, 2]]  # change order to have x,y,confidence
        pose = np.reshape(pose, (self.batch_size, -1), name="output")  # bring into batchsize times x,y,conf etc.

        return pose
    
    def inference_before_pose(self, x):
        heads = self.sess.run(self.model, feed_dict={
            self.input: x
        })
        
        return heads
