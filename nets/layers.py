"""
    Creator: Matteo Ceradini
"""

seed_value = 1234

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['PYTHONHASHSEED'] = str(seed_value)

import numpy as np
np.random.seed(seed_value)

import tensorflow as tf
tf.get_logger().setLevel('ERROR')
tf.compat.v1.set_random_seed(seed_value)
tf.set_random_seed(seed_value)

def conv2d(x, W, strides=1, relu=True, padding='SAME'):
    output = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding=padding)

    if relu:
        output = tf.nn.relu(output)
    
    return output

def batch_norm(x, W):
    mean = W['batch_norm/moving_mean']
    var = W['batch_norm/moving_variance']
    beta = W['batch_norm/beta']
    gamma = W['batch_norm/gamma']

    normed = tf.nn.batch_normalization(x, mean, var, beta, gamma, 0.001)

    return normed

def conv2d_batchnorm(inputs, W, strides=1):   
    conv = conv2d(inputs, W['conv2d'], strides, relu=False) 
    conv = batch_norm(conv, W)

    return conv

def conv2d_batchnorm_relu(inputs, W, strides):
    x = conv2d_batchnorm(inputs, W, strides)

    return tf.nn.relu(x)

def max_pool_2d(x, ksize=2, strides=2):
    return tf.nn.max_pool(x, ksize=[1, ksize, ksize, 1], strides=[1, strides, strides, 1], padding='SAME')

def deconv2d(inputs, W, B, outputs, strides=2):
    dyn_input_shape = tf.shape(inputs)

    batch_size = dyn_input_shape[0]
    out_h = dyn_input_shape[1] * strides
    out_w = dyn_input_shape[2] * strides

    deconv = tf.nn.conv2d_transpose(inputs, W, strides=[1, strides, strides, 1], padding="SAME", output_shape=[batch_size, out_h, out_w, outputs])
    deconv = tf.nn.bias_add(deconv, B)

    return deconv

def deconv2d_static_shape(inputs, W, B, outputs, shape, strides=2):
    deconv = tf.nn.conv2d_transpose(inputs, W, strides=[1, strides, strides, 1], padding="SAME", output_shape=[shape[0], shape[1], shape[2], outputs])
    deconv = tf.nn.bias_add(deconv, B)

    return deconv