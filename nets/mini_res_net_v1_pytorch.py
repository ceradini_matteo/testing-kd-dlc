"""
    Creator: Matteo Ceradini
"""

seed = 1234

import matplotlib.pyplot as plt
import time
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['PYTHONHASHSEED'] = str(seed)

import numpy as np
np.random.seed(seed)

import torch
torch.manual_seed(seed)

import torch.nn as nn
import torch.optim as optim
from torchvision import models

import tensorflow as tf
tf.get_logger().setLevel('ERROR')
tf.compat.v1.set_random_seed(seed)
tf.set_random_seed(seed)

import nets.init.layers_shape_resnet_2_block as layer_shapes

class MiniResNet(nn.Module):
    def __init__(self, args, device, load_dlc_weights):
        super().__init__()
        
        self.device = device

        self.mean_pixel = args['mean_pixel']
        self.batch_size = args['batch_size']
        
        self.num_block_conv_layers = 3
        self.model_stride = 8
        self.layers_to_remove = 2
        self.num_blocks = 2
        self.num_block_units = [3,4]
        self.num_block_conv_layers = 3

        self.resnet_dlc_model = './dlc_models/{}'.format('resnet-50')

        pre_trained_resnet = models.resnet50(pretrained=True)

        # freezing all the resnet
        for child in pre_trained_resnet.children():
            for param in child.parameters():
                param.requires_grad = False
        
        resnet_modules = list(pre_trained_resnet.children())[:-(2+self.layers_to_remove)]

        self.resnet_encoder = nn.Sequential(*resnet_modules)
        
        if load_dlc_weights:
            print('Loading the DeepLabCut model weights from: {}'.format(self.resnet_dlc_model))
            tf_weights = self.__define_tf_weights()
            self.load_tf_weights(tf_weights)

        self.new_conv = nn.Sequential(
            nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=3, stride=2, padding=0, bias=False)
        )

        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(in_channels=1024, out_channels=1, kernel_size=3, stride=2, padding=1, output_padding=(1,1))
        )
        
        self.sigmoid = nn.Sigmoid()

    def input_normalizer(self, x):
        return np.subtract(x, self.mean_pixel)
    
    def forward(self, x):
        # normalization and transpose (from np to torch)
        x = self.input_normalizer(x)        
        x = torch.tensor(np.transpose(x, [0, 3, 2, 1]), dtype=torch.float).to(self.device)

        # resnet blocks
        result = self.resnet_encoder(x)
        
        # new last part (after the ResNet part)
        result = nn.functional.pad(result, [1,1,1,1])
        result = self.new_conv(result)    
        result = self.decoder(result)
        
        # pose inference
        probs = self.sigmoid(result)
        probs = np.transpose(probs.detach().cpu().numpy(), [0, 3, 2, 1])
        
        if self.batch_size == 1: 
            probs = np.squeeze(probs, axis=0)
            l_shape = np.shape(probs)
            probs = np.reshape(probs, (l_shape[0] * l_shape[1], -1))
            maxloc = np.argmax(probs, axis=0)
            loc = np.unravel_index(maxloc, (l_shape[0], l_shape[1]))
            maxloc = np.reshape(maxloc, (1, -1))
            joints = np.reshape(np.arange(0, l_shape[2]), (1, -1))
            indices = np.transpose(np.concatenate([maxloc, joints], axis=0))
            likelihood = np.reshape(probs[indices][0,0], (1,1))
        else:
            probs = np.transpose(probs, [1, 2, 0, 3])
            l_shape = np.shape(probs)
            probs = np.reshape(probs, (l_shape[0] * l_shape[1], -1))
            maxloc = np.argmax(probs, axis=0)
            loc = np.unravel_index(maxloc, (l_shape[0], l_shape[1]))
            maxloc = np.reshape(maxloc, (1, -1))
            joints = np.reshape(np.arange(0, l_shape[2] * l_shape[3]), (1, -1))
            indices = np.transpose(np.concatenate([maxloc, joints], axis=0))
            likelihood = np.zeros([self.batch_size])

            for i in range(0, self.batch_size):
                likelihood[i] = probs[indices[i,0],indices[i,1]]

            likelihood = np.reshape(likelihood, (self.batch_size,1))

        pose = (self.model_stride * np.transpose(loc) + self.model_stride * 0.5)
        pose = np.concatenate([pose, likelihood], axis=1)
        
        pose[:, [0, 1, 2]] = pose[:, [1, 0, 2]]  # change order to have x,y,confidence
        pose = np.reshape(pose, (self.batch_size, -1))  # bring into batchsize times x,y,conf etc.

        return result, pose
    
    def __define_tf_weights(self):
        tf_weights = {
            'conv1':{
                'conv2d': tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/conv1/weights'),
                'batch_norm/moving_mean': tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/conv1/BatchNorm/moving_mean'),
                'batch_norm/moving_variance': tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/conv1/BatchNorm/moving_variance'),
                'batch_norm/gamma': tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/conv1/BatchNorm/gamma'),
                'batch_norm/beta': tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/conv1/BatchNorm/beta')
            }
        }
        # setup resnet shortcut weights and batchnorm operations
        for b in range(0, self.num_blocks):
            tf_weights['block{}'.format(b+1)] = {}

        shortcut_weights_shapes = layer_shapes.get_shortcut_weights_shapes()
        shortcut_batchnorm_shapes = layer_shapes.get_shortcut_batchnorm_shapes()

        for b in range(0, self.num_blocks):
            tf_weights['block{}'.format(b+1)]['unit_1/shortcut'] = {}

            tf_weights['block{}'.format(b+1)]['unit_1/shortcut']['conv2d'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/weights'.format(b+1))
            tf_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/moving_mean'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/BatchNorm/moving_mean'.format(b+1))
            tf_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/moving_variance'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/BatchNorm/moving_variance'.format(b+1))
            tf_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/gamma'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/BatchNorm/gamma'.format(b+1))
            tf_weights['block{}'.format(b+1)]['unit_1/shortcut']['batch_norm/beta'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_1/bottleneck_v1/shortcut/BatchNorm/beta'.format(b+1))

        # setup resnet block weights fr convolutions and batchnorm operations
        block_weights_shapes = layer_shapes.get_block_weights_shapes()
        block_batchnorm_shapes = layer_shapes.get_block_batchnorm_shapes()

        for b in range(0, self.num_blocks):
            for u in range(0, self.num_block_units[b]):
                for c in range(0, self.num_block_conv_layers):
                    tf_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)] = {}

                    tf_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['conv2d'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/weights'.format(b+1,u+1,c+1))
                    tf_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/moving_mean'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/BatchNorm/moving_mean'.format(b+1,u+1,c+1))
                    tf_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/moving_variance'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/BatchNorm/moving_variance'.format(b+1,u+1,c+1))
                    tf_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/gamma'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/BatchNorm/gamma'.format(b+1,u+1,c+1))
                    tf_weights['block{}'.format(b+1)]['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/beta'] = tf.train.load_variable(self.resnet_dlc_model, 'resnet_v1_50/block{}/unit_{}/bottleneck_v1/conv{}/BatchNorm/beta'.format(b+1,u+1,c+1))


        return tf_weights
            
    def __set_block_weights(self, layer, tf_weights, units):        
        for u in range(0, units):
            for c in range(0, self.num_block_conv_layers):
                # convs
                weights_torch = torch.tensor(np.transpose(tf_weights['unit_{}/conv{}'.format(u+1,c+1)]['conv2d'], [3, 2, 1, 0]), dtype=torch.float).to(self.device)
                getattr(layer[u], 'conv{}'.format(c+1)).weight = torch.nn.Parameter(weights_torch)
            
                # batchnorms
                getattr(layer[u], 'bn{}'.format(c+1)).weight.data.copy_(torch.from_numpy(tf_weights['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/gamma']).to(self.device))
                getattr(layer[u], 'bn{}'.format(c+1)).bias.data.copy_(torch.from_numpy(tf_weights['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/beta']).to(self.device))
                getattr(layer[u], 'bn{}'.format(c+1)).running_mean.data.copy_(torch.from_numpy(tf_weights['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/moving_mean']).to(self.device))
                getattr(layer[u], 'bn{}'.format(c+1)).running_var.data.copy_(torch.from_numpy(tf_weights['unit_{}/conv{}'.format(u+1,c+1)]['batch_norm/moving_variance']).to(self.device))

        # shortcut
        weights_torch = torch.tensor(np.transpose(tf_weights['unit_1/shortcut']['conv2d'], [3, 2, 1, 0]), dtype=torch.float).to(self.device)
        layer[0].downsample[0].weight = torch.nn.Parameter(weights_torch)
        
        layer[0].downsample[1].weight.data.copy_(torch.from_numpy(tf_weights['unit_1/shortcut']['batch_norm/gamma']).to(self.device))
        layer[0].downsample[1].bias.data.copy_(torch.from_numpy(tf_weights['unit_1/shortcut']['batch_norm/beta']).to(self.device))
        layer[0].downsample[1].running_mean.data.copy_(torch.from_numpy(tf_weights['unit_1/shortcut']['batch_norm/moving_mean']).to(self.device))
        layer[0].downsample[1].running_var.data.copy_(torch.from_numpy(tf_weights['unit_1/shortcut']['batch_norm/moving_variance']).to(self.device))

    def load_tf_weights(self, tf_weights):
        # first conv
        weights_torch = torch.tensor(np.transpose(tf_weights['conv1']['conv2d'], [3, 2, 1, 0]), dtype=torch.float).to(self.device)
        self.resnet_encoder[0].weight = torch.nn.Parameter(weights_torch)
        self.resnet_encoder[1].weight.data.copy_(torch.from_numpy(tf_weights['conv1']['batch_norm/gamma']).to(self.device))
        self.resnet_encoder[1].bias.data.copy_(torch.from_numpy(tf_weights['conv1']['batch_norm/beta']).to(self.device))
        self.resnet_encoder[1].running_mean.data.copy_(torch.from_numpy(tf_weights['conv1']['batch_norm/moving_mean']).to(self.device))
        self.resnet_encoder[1].running_var.data.copy_(torch.from_numpy(tf_weights['conv1']['batch_norm/moving_variance']).to(self.device))

        # resnet blocks
        # block1
        self.__set_block_weights(self.resnet_encoder[4], tf_weights['block1'], 3)
        # block2
        self.__set_block_weights(self.resnet_encoder[5], tf_weights['block2'], 4)

class ConvNet():
    def __init__(self, args, model_name, load_dlc_weights=False):
        self.batch_size = args['batch_size']
        self.stride = args['stride']
        self.num_outputs = args['num_outputs']
        self.locref_stdev = args['locref_stdev']
        self.new_training = args['new_training']

        self.learning_rate = 0.0001
        self.temp_threshold = 0.8
        self.model_path = "./models/{}".format(model_name)

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.model = MiniResNet(args, self.device, load_dlc_weights)       
        self.model = self.model.to(self.device)        

    def save_model(self):
        torch.save(self.model.state_dict(), self.model_path)

    def load_model(self):
        self.model.load_state_dict(torch.load(self.model_path))

    # define the cost function and the optimizer for the training phase
    def prepare_for_training(self):
        self.loss_fun = nn.MSELoss()
        self.optimizer = optim.Adam(self.model.parameters(), lr=self.learning_rate)
        #self.optimizer = optim.SGD(self.model.parameters(), lr=self.learning_rate, momentum=0.9)
        self.model.train()
        
    def prepare_for_pose_estimation(self):
        self.model.eval()
        
    def my_loss(self, pred, dlc_pred, pose, dlc_pose):
        loss = self.loss_fun(pred, dlc_pred)#0.2 * self.loss_fun(pred, dlc_pred) + 0.8 * self.loss_fun(torch.from_numpy(pose[:,:2]).float().to(self.device), torch.from_numpy(dlc_pose[:,:2]).float().to(self.device))

        return loss

    def train(self, x, dlc_part_pred, dlc_pose, validation):
        self.optimizer.zero_grad()

        part_pred, pose = self.model(x)
        
        dlc_part_pred = np.reshape(dlc_part_pred, (self.batch_size, self.num_outputs, dlc_part_pred.shape[2], dlc_part_pred.shape[1]))
        
        loss = self.my_loss(part_pred, torch.from_numpy(dlc_part_pred).float().to(self.device), pose, dlc_pose)
        loss.backward()

        self.optimizer.step()
        
        if validation:
            pose_loss = self.loss_fun(torch.from_numpy(pose[:,:2]).float().to(self.device), torch.from_numpy(dlc_pose[:,:2]).float().to(self.device))
            print('\nPose loss: {}'.format(pose_loss))
            print('kd pose:', pose)
            print('dlc_pose:', dlc_pose)

        return loss.item()

    def inference(self, x):
        _, pose = self.model(x)

        return pose

    def inference_before_pose(self, x):
        x = np.reshape(x, (self.batch_size, x.shape[3], x.shape[2], x.shape[1]))
        x_input = torch.from_numpy(x).float()

        part_pred, _ = self.model(x_input)

        return part_pred