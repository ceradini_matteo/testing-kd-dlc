import tensorflow as tf
tf.get_logger().setLevel('ERROR')
tf.compat.v1.set_random_seed(1234)

from nets.layers import *

def __projection_block(inputs, W, strides, block_name):
    #left stream
    x = conv2d_batchnorm_relu(inputs, W['unit_1/conv1'], strides=strides)
    x = conv2d_batchnorm_relu(x, W['unit_1/conv2'], strides=1)
    x = conv2d_batchnorm(x, W['unit_1/conv3'], strides=1)

    #right stream
    shortcut = conv2d_batchnorm(inputs, W['unit_1/shortcut'], strides=strides)

    #skip connection
    x = tf.math.add(shortcut, x, name="resnet_v1_50/{}/unit_1/Add".format(block_name))

    return  tf.nn.relu(x, name="resnet_v1_50/{}/unit1/Relu".format(block_name))

def __identity_block(inputs, W, block_name, index):
    x = conv2d_batchnorm_relu(inputs, W['unit_{}/conv1'.format(index)], strides=1)
    x = conv2d_batchnorm_relu(x, W['unit_{}/conv2'.format(index)], strides=1)
    x = conv2d_batchnorm(x, W['unit_{}/conv3'.format(index)], strides=1)

    #skip connection
    x = tf.math.add(inputs, x, name="resnet_v1_50/{}/unit_{}/Add".format(block_name, index))

    return tf.nn.relu(x, name="resnet_v1_50/{}/unit_{}/Relu".format(block_name, index))

def get_resnet_block(x, reps, W, strides, block_name):
    """
    Args:
        x:          Tensor, 4D BHWD input maps
        reps:       number of repetition of this block
        strides:    strides for each block
        block_name: the name for the block to use from the model
        W:          weights from the model pre-trained
    Return:
        block:      res_net block
    """
    block = __projection_block(x, W, strides, block_name)

    for i in range(1, reps):
        block = __identity_block(block, W, block_name=block_name, index=(i+1))

    return block