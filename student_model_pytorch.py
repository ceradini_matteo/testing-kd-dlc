"""
    Creator: Matteo Ceradini
"""

from nets.mini_res_net_v1_pytorch import ConvNet as conv_net

class StudentModel:
    def __init__(self, args, model_name="mini_resnet_v1", training=False, pose_estimation=False):
        print("Initializing the student model (PyTorch)")
        
        self.model_name = model_name

        if training:
            self.conv_net = conv_net(args, model_name, load_dlc_weights=True)
            self.conv_net.prepare_for_training()
        elif pose_estimation:
            self.conv_net = conv_net(args, model_name, load_dlc_weights=False)
            self.conv_net.prepare_for_pose_estimation()

    def inference(self, x):
        return self.conv_net.inference(x)
    
    def inference_before_pose(self, x):
        return self.conv_net.inference_before_pose(x)

    def update_weights(self, x, dlc_prediction, dlc_pose, validation):
        return self.conv_net.train(x, dlc_prediction, dlc_pose, validation)

    def save_model(self):
        self.conv_net.save_model()

    def load_model(self):
        print("Loading the student model (PyTorch)from: {}".format(self.model_name))
        self.conv_net.load_model()